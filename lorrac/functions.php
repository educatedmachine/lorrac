<?php
function my_theme_enqueue_styles() {

    $parent_style = 'edin'; // This is for the Edin theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'lorrac',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
?>

<?php
function search_bar_widget() {

	register_sidebar( array(
		'name'          => 'Search Bar Widget',
		'id'            => 'search_bar_widget',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'search_bar_widget' );
?>